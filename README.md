# Tarification 

# Encadrement 

## definition :
Assistance dans les choix techniques qui n'y implique pas un effort de production ou édition de solution.

le projet sera mis en public (libre, open source) ou propriétaire de l'encadreur selon la convention des deux parties .

Ette offre n'est valable que pour des petits projets sur des durées limitées (inférieur a six mois) 

les frait sont versé au prealable 

##### les projet non libre 
Pour des projets propriétaire les tarifs seront multipliés par 3 
## services offerte : 
### Service prendre le besion :
--> frait de deplacement 
##### tarification :  ( estimable )  

--> Écouter le besoin et faire un questionnaire à propos du projet
##### tarification : 20 Dt / 45min 


### Service encadrement d'un nouveau projet :

--> Étude, cadrage, planning du projet Établir, etdité la documentation, faire les choix technologiques, tous les documents seront l'objet d'un projet git, cette phase ne necessite pas l'intervention du candidat dans le cas d'un nouveau projet 

    -> scénarios possible :
            + l'encadreur propose tout .
            + le candidat propose le besoin et l'encadreur fait les choix technologiques, le planning et écrit la documentation initiale du projet 
            +le candidat propose les techniques et l'encadreur fait apparaitre le besoin et le planning et écrit la documentation initiale du projet
            + le candidat propose le besoin et la technologie l'encadreur fait le planning et écrit la documentation initiale du projet

un livrable sera fait sur git(lab), si implique seulement la première phase
##### tarification : 350 DT (net)

### Service encadrement d'un projet deja debuté ( < 50% ):
--> Étude du projet pour faire une estimation en matière de temps de financement, une recommandation des compétences sera faite si le projet n'est pas dans le domaine de compétence du formateur 

le livrable est l'estimation en matière de temps et argent 
##### tarification : 200 DT (net)

--> Étude, cadrage, planning, reveiw des choix technologiques, éditer de la documentation 

un livrable sera fait sur git(lab), si implique seulement le cadrage du projet
##### tarification : a partir 300 DT (net)


### Service encadrement d'un projet fini sans documentation ( > 50% ):
--> Étude du projet pour faire une estimation en matière de temps de financement, une recommandation des compétences sera faite si le projet n'est pas dans le domaine de compétence du formateur  

le livrable est l'estimation en matière de temps et argent
##### tarification : 200 DT (net)
--> Écrire la documentation les README md globale du projet ( le code doit être commenté);
Étude, cadrage, reveiw des choix technologiques, éditer de la documentation

un livrable sera fait sur git(lab), ça implique seulement le cadrage du projet
##### tarification : a partir 300 DT (net)
-->Écrire la documentation les README md globale du projet pour un projet sans documentation du code , Étude, cadrage, planning, reveiw des choix technologiques, éditer de la documentation 

un livrable sera fait sur git(lab), ça implique seulement le cadrage du projet
##### tarification : ( estimable )


### Service heurs supplimentaire d'encadrement :
--> ce service ne sera possible que pour les consommateurs des services perdants, tout heur supplémentaire sous demande de candidat ou des candidats sera facturé comme suit ;

NB : nombre des candidats = NbC
##### tarification : 20 DT + (5 x NbC) / 45min (net)

### Service Validation technique du rapport :
--> Validation technique du rapport, mettre le point sur la coerance du besoin et les choix adoptés, l'utilisation des bonnes définition et spécification, correction des diagrammes et schémas

le rapport sera mis sur git(lab)
##### tarification : 250 DT (net)


### Service heurs supplimentaire d'encadrement :
--> Validation technique test du code proposition des solutions pas d'implementation de solution 


##### tarification : 100 DT / consultation (net)


# FreeLance

### Service heurs  de freeLance ( < 10 h ) :
--> Cette offre pour les petits projets, le temps de la recherche d'informations, son implementation, les tests, et la validation sont englobées dans ce temps, une pause-record sera mis en cas de suspension des travaux, une solution clef en main sera livrée pour le client, le projet est public sauf l'indication du contraire par le client ceci impliquera l'application de la regle des projets non libres 
 
les znregistrements seront l'objet d'exposition Youtube sur la chaine du freelancer , et le livrable du code avec documentation seront l'objet sur gitlab
##### tarification : 40 à 80 DT / 45 min (net)


### Service jours de freeLance ( < 7 j ) :
--> Cette offre pour les petits projets, le temps de la recherche d'informations, son implementation, les tests, et la validation sont englobées dans ce temps, une solution clef en main sera livrée pour le client, le projet est public sauf l'indication du contraire par le client, ceci impliquera l'application de la regle des projets non libres 
 

le livrable du code avec documentation sera l'objet sur gitlab
NB: 1J au allons tour de  5h 
##### tarification : 150 à 300 DT / jour (net)


### Service freeLance par projet ( > 7 j ) :
--> un projet passera par les phases d'encadrement pour faire le cadrage du projet, un produit fini sera mis a disposition .
un ajout d'autres freelancer est possible et négociable

##### tarification : ( estimable )


### passation et formation :
--> uun transfère de compétence et passation du projet 

NB : nombre des condidats = NbC
##### tarification : 30 DT + ( 5 DT + NbC )

# Formation

### Service Cours de formation 
--> le formateur s'engage à faire une formation selon le planning proposé par lui-même , avec un nombre d'heurs bien définis au préalable, cette offre concerne seulement les groupes chaque formation disponible possède une cota avec laquelle son prix est facturé, le prix est calculable également selon le nombre de candidats  

ces prix sont (nets) sans comptabilisation des frais de déplacement 

##### tarification 

    +-------------------------+------------------------+-------------------+---------------------------+
    |        formation        |  prix (< 3 candidat )  |  nombre d'heurs   |  prix ( > 2 candidat )    |
    +-------------------------+------------------------+-------------------+---------------------------+
    |      DevOps LPI701      |        2000 DT         |        50 h       |   + 250 DT / candidat     |
    +-------------------------+------------------------+-------------------+---------------------------+
    | aws architect associate |          -             |         -         |             -             |
    +-------------------------+------------------------+-------------------+---------------------------+
    |       LPI 101-102       |          -             |         -         |             -             |
    +-------------------------+------------------------+-------------------+---------------------------+
    |       LPI 201-202       |          -             |         -         |             -             |
    +-------------------------+------------------------+-------------------+---------------------------+
    |          CKAD           |          -             |         -         |             -             |
    +-------------------------+------------------------+-------------------+---------------------------+
    |      Ansible RedHat     |          -             |         -         |             -             |
    +-------------------------+------------------------+-------------------+---------------------------+


# Conseil proffessionnel 


### Service Conseille technique : 
--> ce service pour les personnes qui trouves des difficultés techniques dans leurs milieux professionnels , 

##### tarification : 60 DT / 45min 

### Service Conseille cariere : 
--> ce service pour les personnes qui trouves des difficultés dans choix de carriere  

##### tarification : 80 DT / 45min